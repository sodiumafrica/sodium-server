<!doctype html>
<html lang="en">
<head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css"
          integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">

    <title>Sodium Server | Production</title>
</head>
<body>

<div class="container">
    <div class="row">

        <div class="col-6 offset-3 pt-5">
            <div class="card">
                <div class="card-body text-center">
                    <h1 class="card-title">Sodium Server</h1>
                    <p class="card-text">A production-ready PHP web server for your heavy workloads</p>
                </div>
                <ul class="list-group list-group-flush">
                    <li class="list-group-item list-group-item-action flex-column align-items-start">
                        <small>Release</small>
                        <p class="mb-1">1.0.0</p>
                    </li>
                    <li class="list-group-item list-group-item-action flex-column align-items-start">
                        <small>Base Image</small>
                        <p class="mb-1">php:7.2.4-fpm-alpine</p>
                    </li>
                    <li class="list-group-item list-group-item-action flex-column align-items-start">
                        <small>Web Server</small>
                        <p class="mb-1">Nginx - Version 1.14.0</p>
                    </li>
                    <li class="list-group-item list-group-item-action flex-column align-items-start">
                        <small>PHP Package Manager</small>
                        <p class="mb-1">Composer - Latest Version (on build)</p>
                    </li>
                </ul>
                <div class="card-body text-center">
                    This webserver image is derivative works from
                    <br>
                    <code>richarvery/nginx-php-fpm</code>
                    <br>
                    Enjoy this release!
                    <br>
                    <br>
                    <a target="_blank"
                       href="https://bitbucket.org/sodiumafrica/sodium-server"
                       class="btn btn-primary">
                        View documentation
                    </a>
                </div>
            </div>
        </div>

    </div>
</div>

</body>
</html>

<style>
    body {
        background: #403B4A; /* fallback for old browsers */
        background: -webkit-linear-gradient(to right, #E7E9BB, #403B4A); /* Chrome 10-25, Safari 5.1-6 */
        background: linear-gradient(to right, #E7E9BB, #403B4A); /* W3C, IE 10+/ Edge, Firefox 16+, Chrome 26+, Opera 12+, Safari 7+ */
    }
</style>