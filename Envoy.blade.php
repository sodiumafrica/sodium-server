@servers(['local'=>'127.0.0.1', 'production'=>'root@barua.irenkenya.com'])

{{--Deploy application--}}
@story('deploy')
push-to-git
pull-to-live
{{--build-complete--}}
@endstory

{{--Pull new changes from remote repo to VPS --}}
@task('pull-to-live', ['on'=>'production'])
cd /root/sodium-server
git fetch --all
git reset --hard origin/master
@endtask

{{--Push local changes to remote Git repo--}}
@task('push-to-git', ['on'=>'local'])
git add .
git commit -m "This is an automated deployment"
git push -u origin master
@endtask

@task('build-complete', ['on'=>'local'])
{{--notify-send 'Sodium Server Deployed!' 'Server updates have been successfully deployed'--}}
{{--say 'Sodium Server Deployed!'--}}
@endtask
