## Introduction
Sodium Server is a production-ready webserver built with Nginx and PHP-FPM. It also comes packed with 
Composer, pluggable Nginx server blocks, Supervisor, wget, curl and git. The following PHP extensions are also available:-

`iconv pdo_mysql pdo_sqlite mysqli gd exif intl xsl json soap dom zip opcache`

I decided to move all our 
customers and SaaS products to Docker hosting after a stretch of stressing with bare-metal VPS 
management. After several months of testing this image(on Lavarel projects), I am confident it 
works well for the wild.

## Building the image
Run the following command to build a Docker image from this repository:-
```
docker build -t {} 
```

## Using with Docker Compose
This image works nicely in a `docker-compose.yml` file since your application will depend on other services 
e.g. MySQL or Redis to work. 

Here is a sample 'docker-compose.yml' section:-

<pre>
...
services:
    web:
        image: sodiumafrica/sodiumserver:1.0
        volumes:
            - ./path/to/vhost.conf:/etc/nginx/servers/website.conf:rw
            - ./www/:/var/www:rw
        ports:
          - 15000:80 
...
</pre>

The image will automatically load any `*.conf` files found in `/etc/nginx/servers/`. 
This is usually a good place to mount your server block
You can use [this Nginx server block](https://bitbucket.org/sodiumafrica/sodium-server/src/master/docker/vhost.conf) 
to kickstart your project.

## Demo Server
You can see a demo server running [here](http://212.71.247.115:15000).